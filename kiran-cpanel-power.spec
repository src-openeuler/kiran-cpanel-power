Name:    kiran-cpanel-power
Version: 2.2.0
Release: 4
Summary: Kiran Power Manager
Summary(zh_CN): 电源管理

License: MulanPSL-2.0
Source0: %{name}-%{version}.tar.gz

Patch0001: 0000-feature-upower-glib-yse-UPower-glib-to-determine-whe-b09567c6.patch
Patch0002: 0001-refactor-idle-time-Modify-the-setting-form-of-idle-t-40aeb96c.patch
Patch0003: 0002-refactor-view-hide-configuration-items-made-by-suspe-b42c7200.patch
Patch0004: 0003-fix-brightness-abnormal-settings-brightness-8406922f.patch
Patch0005: 0004-fix-view-fix-ui-description-error-f3096927.patch
Patch0006: 0005-fix-brightness-set-minmum-brightness-to-20-957b0262.patch
Patch0007: 0007-refactor-license-update-license-8996b258.patch
Patch0008: 0008-fix-slider_idleTime-fix-slider-no-updated-and-Macro--92293804.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtsvg-devel
BuildRequires: kiran-widgets-qt5-devel
BuildRequires: kiran-cc-daemon-devel
BuildRequires: kiran-control-panel-devel
BuildRequires: kiran-log-qt5-devel
BuildRequires: gsettings-qt-devel
BuildRequires: qt5-linguist
BuildRequires: upower-devel

Requires: qt5-qtbase
Requires: qt5-qtsvg
Requires: kiran-widgets-qt5
Requires: kiran-session-daemon
Requires: kiran-cpanel-launcher
Requires: kiran-log-qt5
Requires: gsettings-qt
Requires: kiran-screensaver >= 2.2.0-6
Requires: upower
Requires: gtk-update-icon-cache

%description
%{summary}.

%prep
%autosetup -p1

%build
%{__mkdir} -p %{buildroot}
%cmake
%make_build

%install
%make_install

%post
gtk-update-icon-cache -f /usr/share/icons/hicolor/

%files
%doc
%{_datadir}/kiran-control-panel/plugins/libs/libkiran-cpanel-power.so*
%{_datadir}/kiran-control-panel/plugins/desktop/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*
%{_datadir}/kiran-cpanel-power/translations/*.qm

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 10 2022 luoqing <luoqing@kylinsec.com.cn> - 2.2.0-4
- KYOS-F: Modify license and add yaml file.

* Tue Mar 01 2022 longcheng <longcheng@kylinos.com.cn> - 2.2.0-3
- KYOS-B: Add Requires gtk-update-icon-cache

* Thu Feb 24 2022 chendingjian <chendingjian@kylinos.com.cn> - 2.2.0-2
- rebuild for KY3.4-MATE-modules-dev

* Mon Feb 21 2022 luoqing <luoqing@kylinos.com.cn> - 2.2.0-1.kb9
- KYOS-B: fix slider no updated (#50147)

* Mon Jan 24 2022 liuxinhao <liuxinhao@kylinos.com.cn> - 2.2.0-1.kb7
- KYBD: update license

* Wed Jan 19 2022 liuxinhao <liuxinhao@kylinos.com.cn> - 2.2.0-1.kb6
- KYOS-F: using upower-glib detect battery
- KYOS-F: refactor idle time
- KYOS-B: fix brightness abnormal settings (#48486)

* Wed Dec 29 2021 kpkg <kpkg@kylinos.com.cn> - 2.2.0-1.kb1
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 caoyuanji<caoyuanji@kylinos.com.cn> - 2.2.0-1
- Upgrade version number for easy upgrade

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-0.kb3
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-0.kb2
- rebuild for KY3.4-4-KiranUI-2.2

* Fri Oct 15 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.2.0-0.kb1
- Initial build
